#!/usr/bin/make -f

OTRSHOME := /usr/share/otrs
DB_BASE  := /usr/share/dbconfig-common
DB_DIR   := $(DB_BASE)/data/otrs2
OTRS_DST := $(CURDIR)/debian/otrs2/

%:
	dh $@

override_dh_install:
	cp scripts/apache2-httpd.include.conf debian/otrs2.conf
	dh_install
	# Setup dbconfig-common
	# PostgreSQL
	cat scripts/database/otrs-schema.postgresql.sql \
	    scripts/database/otrs-initial_insert.postgresql.sql \
		scripts/database/otrs-schema-post.postgresql.sql > \
	    $(OTRS_DST)$(DB_DIR)/install/pgsql
	cp  debian/schemas/DBUpdate-to-3.2.postgresql.sql \
		$(OTRS_DST)$(DB_DIR)/upgrade/pgsql/3.2.1
	cp	scripts/DBUpdate-to-3.3.postgresql.sql \
		$(OTRS_DST)$(DB_DIR)/upgrade/pgsql/3.3.0
	cp  debian/schemas/DBUpdate-to-3.2.1 \
		$(OTRS_DST)$(DB_BASE)/scripts/otrs2/upgrade/pgsql/3.2.1
	cp  debian/schemas/DBUpdate-to-3.3.1 \
		$(OTRS_DST)$(DB_BASE)/scripts/otrs2/upgrade/pgsql/3.3.1
	# MySQL
	cat scripts/database/otrs-schema.mysql.sql \
	    scripts/database/otrs-initial_insert.mysql.sql \
		scripts/database/otrs-schema-post.mysql.sql > \
	    $(OTRS_DST)$(DB_DIR)/install/mysql
	cp  debian/schemas/DBUpdate-to-3.2.mysql.sql \
		$(OTRS_DST)$(DB_DIR)/upgrade/mysql/3.2.1
	cp	scripts/DBUpdate-to-3.3.mysql.sql \
		$(OTRS_DST)$(DB_DIR)/upgrade/mysql/3.3.0
	cp  debian/schemas/DBUpdate-to-3.2.0 \
		$(OTRS_DST)$(DB_BASE)/scripts/otrs2/upgrade/mysql/3.2.0
	cp  debian/schemas/DBUpdate-to-3.2.1 \
		$(OTRS_DST)$(DB_BASE)/scripts/otrs2/upgrade/mysql/3.2.1
	cp  debian/schemas/DBUpdate-to-3.3.1 \
		$(OTRS_DST)$(DB_BASE)/scripts/otrs2/upgrade/mysql/3.3.1
	# I guess we do not need {redhat,suse,fedora}* stuff in Debian
	rm -f $(OTRS_DST)$(OTRSHOME)/scripts/fedora* \
	      $(OTRS_DST)$(OTRSHOME)/scripts/redhat* \
	      $(OTRS_DST)$(OTRSHOME)/scripts/rpc-example.pl \
	      $(OTRS_DST)$(OTRSHOME)/scripts/suse* \
	      $(OTRS_DST)$(OTRSHOME)/scripts/otrs*gentoo* \
		  $(OTRS_DST)$(OTRSHOME)/scripts/auto_build.sh \
	      $(OTRS_DST)$(OTRSHOME)/bin/Cron4Win32.pl
	# Remove some thirdparty directories, where we link against the
	# system version.
	rm -rf	$(OTRS_DST)/var/lib/otrs/httpd/htdocs/js/thirdparty/jquery-ui-1.*
	# Remove embedded ttf-dejavu fonts.
	rm $(OTRS_DST)/var/lib/otrs/fonts/DejaVu*.ttf
	# Remove embedded fonts-font-awesome fonts.
	rm $(OTRS_DST)/var/lib/otrs/httpd/htdocs/skins/Agent/default/css/thirdparty/fontawesome/fontawesome-webfont.ttf
	rm $(OTRS_DST)/var/lib/otrs/httpd/htdocs/skins/Customer/default/css/thirdparty/fontawesome/fontawesome-webfont.ttf
	# Remove extra license file.
	rm $(OTRS_DST)/var/lib/otrs/httpd/htdocs/js/thirdparty/ckeditor-*/LICENSE.md
	rm $(OTRS_DST)/var/lib/otrs/httpd/htdocs/js/thirdparty/ckeditor-*/plugins/codesnippet/lib/highlight/LICENSE
	# Remove embedded pre-minified files and minify them again from source.
	rm $(OTRS_DST)/var/lib/otrs/httpd/htdocs/js/thirdparty/fullcalendar-1.6.1/fullcalendar.min.js
	rm $(OTRS_DST)/var/lib/otrs/httpd/htdocs/js/thirdparty/d3js/d3.v3.min.js
	rm $(OTRS_DST)/var/lib/otrs/httpd/htdocs/js/thirdparty/d3js/nv.d3.min.js
	yui-compressor --type js -o $(OTRS_DST)/var/lib/otrs/httpd/htdocs/js/thirdparty/fullcalendar-1.6.1/fullcalendar.min.js \
		$(OTRS_DST)/var/lib/otrs/httpd/htdocs/js/thirdparty/fullcalendar-1.6.1/fullcalendar.js
	yui-compressor --type js -o $(OTRS_DST)/var/lib/otrs/httpd/htdocs/js/thirdparty/d3js/d3.v3.min.js \
		$(OTRS_DST)/var/lib/otrs/httpd/htdocs/js/thirdparty/d3js/d3.js
	yui-compressor --type js -o $(OTRS_DST)/var/lib/otrs/httpd/htdocs/js/thirdparty/d3js/nv.d3.min.js \
		$(OTRS_DST)/var/lib/otrs/httpd/htdocs/js/thirdparty/d3js/nv.d3.js

override_dh_installchangelogs:
	dh_installchangelogs CHANGES.md
