#!/bin/sh

set -e

OTRSHOME=/usr/share/otrs
OTRS_VAR=/var/lib/otrs
CONFIGFILES="Kernel/Config.pm Kernel/Config/GenericAgent.pm"

setup_database(){
	. /usr/share/dbconfig-common/dpkg/postinst
	dbc_generate_include="perl:/etc/otrs/database.pm"
	dbc_generate_include_owner="otrs:www-data"
	dbc_generate_include_perms="0640"
	dbc_first_version="2.0.4p01-10"
	dbc_mysql_createdb_encoding="UTF8"
	dbc_go otrs2 "$@"
}

setup_ZZZ(){
	# Create the needed ZZZAuto package, if it is not available.
	if [ ! -f $OTRS_VAR/Config/Files/ZZZAuto.pm ]; then
		echo 'package Kernel::Config::Files::ZZZAuto;
sub Load {
	my ($File, $Self) = @_;
}
1;' \
			> $OTRS_VAR/Config/Files/ZZZAuto.pm
		chown root:www-data $OTRS_VAR/Config/Files/ZZZAuto.pm
		chmod 0660 $OTRS_VAR/Config/Files/ZZZAuto.pm
	fi
}

setup_cron(){
	# register the new cron config
	TMPFILE=$(mktemp)
	cat /var/lib/otrs/cron/* > $TMPFILE
	ucf --three-way --debconf-ok $TMPFILE /etc/otrs/cron
	rm -f $TMPFILE
	if [ ! -e /etc/cron.d/otrs2 ] && [ ! -e /etc/cron.d/otrs ]; then
		ln -s /etc/otrs/cron /etc/cron.d/otrs2
	fi
}

setup_apache(){
	if [ -e /usr/share/apache2/apache2-maintscript-helper ] ; then
		. /usr/share/apache2/apache2-maintscript-helper
		apache2_invoke enmod perl
		apache2_invoke enmod rewrite
		apache2_invoke enmod headers
		apache2_invoke enmod deflate
	fi
	db_stop
}

. /usr/share/debconf/confmodule

case "$1" in
	configure|reconfigure)
		if [ -d $OTRS_VAR/Config_deb_upgrade ]; then
			mv $OTRS_VAR/Config_deb_upgrade/ZZZAuto.pm $OTRS_VAR/Config/Files/
			mv $OTRS_VAR/Config_deb_upgrade/ZZZAAuto.pm $OTRS_VAR/Config/Files/
			rm -rf $OTRS_VAR/Config_deb_upgrade
		fi
		setup_database "$@"
		for FILE in $CONFIGFILES; do
			ucf --three-way --debconf-ok $OTRSHOME/$FILE.dist /etc/otrs/$FILE
		done
		setup_cron
		ucf --three-way --debconf-ok $OTRSHOME/.fetchmailrc.dist /etc/otrs/fetchmailrc
		chmod 600 /etc/otrs/fetchmailrc
		setup_apache $@
		setup_ZZZ
		if [ -f /etc/otrs/database.pm ]; then
			cd $OTRSHOME && ./bin/otrs.RebuildConfig.pl > /dev/null
			cd $OTRSHOME && ./bin/otrs.DeleteCache.pl > /dev/null || true
		fi
		if [ -f $OTRS_VAR/Config/Files/ZZZAAuto.pm ]; then
			chown root:www-data $OTRS_VAR/Config/Files/ZZZAAuto.pm
		fi
		chown root:www-data $OTRS_VAR/Config
		chown root:www-data $OTRS_VAR/Config/Files
		chown otrs:www-data /var/spool/otrs
		chmod 0775 $OTRS_VAR/Config/
		chmod 0775 $OTRS_VAR/Config/Files
		chmod 0775 /var/spool/otrs
		# Set required permissions, which were set by otrs.SetPermissions.pl in the past
		if [ ! -f $OTRS_VAR/log/TicketCounter.log ]; then
			echo '' > $OTRS_VAR/log/TicketCounter.log
			chown otrs:www-data $OTRS_VAR/log/TicketCounter.log
			chmod 0664 $OTRS_VAR/log/TicketCounter.log
		fi
		chown otrs:www-data $OTRS_VAR/article
		chmod 2775 $OTRS_VAR/article
		chown otrs:www-data $OTRS_VAR/log
		chmod 2775 $OTRS_VAR/log
		chown otrs:www-data $OTRS_VAR/sessions
		chmod 2775 $OTRS_VAR/sessions
		chown otrs:www-data -R $OTRS_VAR/stats
		chmod 2775 $OTRS_VAR/stats
		chown otrs:www-data -R $OTRS_VAR/tmp
		chmod 0660 /var/lib/otrs/Config/Files/ZZZ*.pm
		chown www-data:www-data /var/lib/otrs/Config/Files/ZZZ*.pm
		find $OTRS_VAR/tmp -type d -exec chmod 2775 {} \;
		find $OTRS_VAR/httpd/htdocs/ -type d -name \*-cache -exec chown otrs:www-data {} \;
		find $OTRS_VAR/httpd/htdocs/ -type d -name \*-cache -exec chmod 2775 {} \;
	;;
	install)
	;;
	*)
		echo "postinst called with unknown argument \`$1'" 1>&2
		exit 1
	;;
esac

if [ -e /usr/share/apache2/apache2-maintscript-helper ] ; then
	. /usr/share/apache2/apache2-maintscript-helper
	apache2_invoke enconf otrs2.conf
fi

#DEBHELPER#
